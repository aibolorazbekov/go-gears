package errors

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/aibolorazbekov/go-gears/logging"
	"gitlab.com/aibolorazbekov/go-gears/logging/keys"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var grpcToHttp = map[codes.Code]int{
	codes.OK:                 200,
	codes.Canceled:           499,
	codes.Unknown:            500,
	codes.InvalidArgument:    400,
	codes.DeadlineExceeded:   504,
	codes.NotFound:           404,
	codes.AlreadyExists:      409,
	codes.PermissionDenied:   403,
	codes.Unauthenticated:    401,
	codes.ResourceExhausted:  429,
	codes.FailedPrecondition: 400,
	codes.Aborted:            409,
	codes.OutOfRange:         400,
	codes.Unimplemented:      501,
	codes.Internal:           500,
	codes.Unavailable:        503,
	codes.DataLoss:           500,
}

var httpToGrpc = map[int]codes.Code{
	200: codes.OK,
	499: codes.Canceled,
	400: codes.InvalidArgument,
	504: codes.DeadlineExceeded,
	404: codes.NotFound,
	409: codes.AlreadyExists,
	403: codes.PermissionDenied,
	401: codes.Unauthenticated,
	429: codes.ResourceExhausted,
	501: codes.Unimplemented,
	500: codes.Internal,
	503: codes.Unavailable,
}

// GrpcWithMsg creates a new GRPC error based on the provided error, code and message.
// The error will be appended to the message (which should not contain a placeholder).
func GrpcWithMsg(err error, code codes.Code, message string) error {
	return status.Error(code, fmt.Sprintf(message+", error: %v", err))
}

func LogAndReturnHttpError(rw http.ResponseWriter, l logging.ContextLogger, err error, msg string, fns ...func(le *logEvent)) {
	le := logEvent{}
	for _, fn := range fns {
		fn(&le)
	}

	if le.code == 0 {
		le.code = http.StatusInternalServerError
	}
	s, ok := status.FromError(err)
	if ok {
		if hCode, found := grpcToHttp[s.Code()]; found {
			le.code = hCode
		}
	}

	WithField(keys.Error, err)(&le)
	l.Errorfl(msg, le.fields)

	er := &errorResponder{
		httpCode:   le.code,
		errMessage: msg,
	}
	er.WriteResponse(rw)
}

type errorResponder struct {
	httpCode   int
	errMessage string
	log        logging.FluentDLogger
}

func (responder *errorResponder) toErrorModel() *errorModel {
	return &errorModel{
		Data: &ErrorData{
			Message: responder.errMessage,
		},
	}
}

func (responder *errorResponder) WriteResponse(rw http.ResponseWriter) {
	errorModel := responder.toErrorModel()
	rw.Header().Set("Content-Type", "application/json")
	rw.WriteHeader(responder.httpCode)
	b, err := json.Marshal(&errorModel)
	if err != nil {
		responder.log.Errorfl("could not marshall http error message", logging.Fields{keys.Error: errorModel.Data.Message})
	}
	_, err = rw.Write(b)
	if err != nil {
		responder.log.Errorfl("could not write http error message", logging.Fields{keys.Error: errorModel.Data.Message})
	}
}

type errorModel struct {
	Data *ErrorData `json:"error"`
}

type ErrorData struct {
	Message string `json:"message,omitempty"`
}

func LogAndReturnError(l logging.ContextLogger, err error, msg string, fns ...func(le *logEvent)) error {
	le := logEvent{
		code: http.StatusInternalServerError,
	}
	for _, fn := range fns {
		fn(&le)
	}

	WithField(keys.Error, err)(&le)
	l.Errorfl(msg, le.fields)

	var grpcCode codes.Code
	if hCode, found := httpToGrpc[le.code]; found {
		grpcCode = hCode
	} else {
		grpcCode = codes.Internal
	}

	return GrpcWithMsg(err, grpcCode, msg)
}

type logEvent struct {
	code   int
	fields logging.Fields
}

func WithField(key string, value interface{}) func(le *logEvent) {
	return func(le *logEvent) {
		if le.fields == nil {
			le.fields = make(logging.Fields)
		}
		le.fields[key] = value
	}
}

func WithFields(fields logging.Fields) func(le *logEvent) {
	return func(le *logEvent) {
		if le.fields == nil {
			le.fields = make(logging.Fields)
		}
		for k, v := range fields {
			le.fields[k] = v
		}
	}
}

func WithCode(code int) func(le *logEvent) {
	return func(le *logEvent) {
		le.code = code
	}
}
