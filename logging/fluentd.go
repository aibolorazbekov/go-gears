package logging

import (
	"fmt"
	"reflect"
	"runtime"
	"strings"

	joonix "github.com/crystal-construct/log"
	"github.com/sirupsen/logrus"
	"gitlab.com/aibolorazbekov/go-gears/logging/keys"
)

type empty struct{}

var Pack = reflect.TypeOf(empty{}).PkgPath()

var emptyFields = func() Fields {
	return Fields{keys.OrderId: ""}
}

type Fields map[string]interface{}

type Logger interface {
	Debug(args ...interface{})
	Debugf(format string, args ...interface{})
	Error(args ...interface{})
	Errorf(format string, args ...interface{})
	Fatal(args ...interface{})
	Fatalf(format string, args ...interface{})
	Info(args ...interface{})
	Infof(format string, args ...interface{})
	Panic(args ...interface{})
	Panicf(format string, args ...interface{})
	Warn(args ...interface{})
	Warnf(format string, args ...interface{})
}

type FluentDLogger interface {
	Logger
	Fatalfl(message string, fields Fields)
	Panicfl(message string, fields Fields)
	Errorfl(message string, fields Fields)
	ErrorFmt(err error, message string, fields Fields) error
	Warnfl(message string, fields Fields)
	Infofl(message string, fields Fields)
	Debugfl(message string, fields Fields)
	GetLogrusLogger() *logrus.Logger
}

func NewDefaultLogger() FluentDLogger {
	log := create("debug")
	return newFluentDLogger(log)
}

func NewDefaultLoggerWithKafkaHook(brokers []string) FluentDLogger {
	log := createWithKafkaHook("debug", brokers)
	return newFluentDLogger(log)
}

func newFluentDLogger(log Logger) FluentDLogger {
	l := &fluentDLogger{
		Logger: log,
	}
	switch log.(type) {
	case *logrus.Logger:
		l.isFluent = true
		joonix.UseStackdriverSeverity()
		log.(*logrus.Logger).Formatter = &joonix.FluentdFormatter{}
	}
	return l
}

type fluentDLogger struct {
	Logger
	isFluent bool
}

func (log *fluentDLogger) Fatalfl(message string, fields Fields) {
	log.logEvent(logrus.FatalLevel, message, fields)
}

func (log *fluentDLogger) Panicfl(message string, fields Fields) {
	log.logEvent(logrus.PanicLevel, message, fields)
}

func (log *fluentDLogger) Errorfl(message string, fields Fields) {
	log.logEvent(logrus.ErrorLevel, message, fields)
}

func (log *fluentDLogger) ErrorFmt(err error, message string, fields Fields) error {
	if fields == nil {
		fields = Fields{}
	}
	fields[keys.Error] = err
	log.logEvent(logrus.ErrorLevel, message, fields)
	return err
}

func (log *fluentDLogger) Warnfl(message string, fields Fields) {
	log.logEvent(logrus.WarnLevel, message, fields)
}
func (log *fluentDLogger) Infofl(message string, fields Fields) {
	log.logEvent(logrus.InfoLevel, message, fields)
}

func (log *fluentDLogger) Debugfl(message string, fields Fields) {
	log.logEvent(logrus.DebugLevel, message, fields)
}

func (log *fluentDLogger) Fatalf(format string, args ...interface{}) {
	log.Fatalfl(fmt.Sprintf(format, args...), emptyFields())
}

func (log *fluentDLogger) Panicf(format string, args ...interface{}) {
	log.Panicfl(fmt.Sprintf(format, args...), emptyFields())
}

func (log *fluentDLogger) Errorf(format string, args ...interface{}) {
	log.Errorfl(fmt.Sprintf(format, args...), emptyFields())
}

func (log *fluentDLogger) Warnf(format string, args ...interface{}) {
	log.Warnfl(fmt.Sprintf(format, args...), emptyFields())
}
func (log *fluentDLogger) Infof(format string, args ...interface{}) {
	log.Infofl(fmt.Sprintf(format, args...), emptyFields())
}

func (log *fluentDLogger) Debugf(format string, args ...interface{}) {
	log.Debugfl(fmt.Sprintf(format, args...), emptyFields())
}
func (log *fluentDLogger) Debug(args ...interface{}) {
	log.Debugfl(fmt.Sprint(args...), emptyFields())
}
func (log *fluentDLogger) Error(args ...interface{}) {
	log.Errorfl(fmt.Sprint(args...), emptyFields())
}
func (log *fluentDLogger) Fatal(args ...interface{}) {
	log.Fatalfl(fmt.Sprint(args...), emptyFields())
}
func (log *fluentDLogger) Info(args ...interface{}) {
	log.Infofl(fmt.Sprint(args...), emptyFields())
}
func (log *fluentDLogger) Panic(args ...interface{}) {
	log.Panicfl(fmt.Sprint(args...), emptyFields())
}
func (log *fluentDLogger) Warn(args ...interface{}) {
	log.Warnfl(fmt.Sprint(args...), emptyFields())
}

func (log *fluentDLogger) logEvent(level logrus.Level, message string, fields map[string]interface{}) {
	fields = defaultVal(fields)

	if log.isFluent {
		lrus := log.Logger.(*logrus.Logger)
		if level > lrus.Level {
			return
		}
		if level <= logrus.ErrorLevel {
			fields["_func"] = log.caller("logging.")
		}
		evt := lrus.WithFields(fields)
		evt.Level = level
		//evt.Message = message
		switch level {
		case logrus.FatalLevel:
			evt.Fatal(message)
		case logrus.PanicLevel:
			evt.Panic(message)
		case logrus.ErrorLevel:
			evt.Error(message)
		case logrus.WarnLevel:
			evt.Warn(message)
		case logrus.InfoLevel:
			evt.Info(message)
		case logrus.DebugLevel:
			evt.Debug(message)
		}
	} else {
		var values string
		for k, v := range fields {
			values += fmt.Sprintf(" %s:%+v", k, v)
		}
		switch level {
		case logrus.FatalLevel:
			log.Fatal(message, values)
		case logrus.PanicLevel:
			log.Panic(message, values)
		case logrus.ErrorLevel:
			log.Panic(message, values)
		case logrus.WarnLevel:
			log.Warn(message, values)
		case logrus.InfoLevel:
			log.Info(message, values)
		case logrus.DebugLevel:
			log.Debug(message, values)
		}
	}
}

func (log *fluentDLogger) caller(ignore string) string {

	// we get the callers as uintptrs - but we just need 1
	fpcs := make([]uintptr, 1)

	skip := 1
	// skip 3 levels to get to the caller of whoever called Caller()
	n := runtime.Callers(skip, fpcs)
	frames := runtime.CallersFrames(fpcs[:n])

	if n == 0 {
		return "n/a" // proper error here would be better
	}

	for {
		frame, more := frames.Next()

		fun := frame.Func
		if fun == nil {
			return "n/a"
		}
		if !strings.Contains(fun.Name(), ignore) {
			return fun.Name()
		}
		if !more {
			return "n/a"
		}
	}
}

func (log *fluentDLogger) GetLogrusLogger() *logrus.Logger {
	if log.isFluent {
		return log.Logger.(*logrus.Logger)
	} else {
		ret := logrus.New()
		ret.Level = logrus.TraceLevel
		return ret
	}
}
