package logging

func defaultVal(fld Fields) Fields {
	if fld == nil {
		fld = Fields{}
	}
	return fld
}
