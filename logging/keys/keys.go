package keys

const (
	Error       = "error"
	OrderId     = "orderId"
	EcomOrderId = "ecomOrderId"
	EcomToken   = "ecomToken"
	PlanDate    = "planDate"
	ClientId    = "clientId"
	PaymentId   = "paymentId"
)
