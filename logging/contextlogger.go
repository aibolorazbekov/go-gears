package logging

import (
	"context"
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/aibolorazbekov/go-gears/ctxutil"
	"gitlab.com/aibolorazbekov/go-gears/logging/keys"
)

// NewFluentContextLogger creates a new context enhanced fluent logger
func NewFluentContextLogger(ctx context.Context, logger FluentDLogger) ContextLogger {
	l := &fluentContextLogger{
		ctx:        ctx,
		flogger:    logger,
		packageTag: "",
	}
	return l
}

// ContextLogger exposes all of the functionality of fluent logger and adds an additional method to provide enhanced logging for errors.
type ContextLogger interface {
	FluentDLogger
}

type fluentContextLogger struct {
	ctx        context.Context
	flogger    FluentDLogger
	packageTag string
}

func (l *fluentContextLogger) Fatalfl(message string, fields Fields) {
	if fields == nil {
		fields = Fields{}
	}
	l.decorateFromContext(fields)
	l.flogger.Fatalfl(message, fields)
}

func (l *fluentContextLogger) Panicfl(message string, fields Fields) {
	if fields == nil {
		fields = Fields{}
	}
	l.decorateFromContext(fields)
	l.flogger.Panicfl(message, fields)
}

func (l *fluentContextLogger) Errorfl(message string, fields Fields) {
	if fields == nil {
		fields = Fields{}
	}
	l.decorateFromContext(fields)
	l.flogger.Errorfl(message, fields)
}

func (l *fluentContextLogger) ErrorFmt(err error, message string, fields Fields) error {
	if fields == nil {
		fields = Fields{}
	}
	l.decorateFromContext(fields)
	fields[keys.Error] = err
	l.flogger.Errorfl(message, fields)
	return err
}

func (l *fluentContextLogger) Warnfl(message string, fields Fields) {
	if fields == nil {
		fields = Fields{}
	}
	l.decorateFromContext(fields)
	l.flogger.Warnfl(message, fields)
}

func (l *fluentContextLogger) Infofl(message string, fields Fields) {
	if fields == nil {
		fields = Fields{}
	}
	l.decorateFromContext(fields)
	l.flogger.Infofl(message, fields)
}

func (l *fluentContextLogger) Debugfl(message string, fields Fields) {
	if fields == nil {
		fields = Fields{}
	}
	l.decorateFromContext(fields)
	l.flogger.Debugfl(message, fields)
}

func (l *fluentContextLogger) newCtxFields() Fields {
	fields := Fields{}
	l.decorateFromContext(fields)
	return fields
}

func (l *fluentContextLogger) Fatalf(format string, args ...interface{}) {
	l.Fatalfl(fmt.Sprintf(format, args...), l.newCtxFields())
}

func (l *fluentContextLogger) Panicf(format string, args ...interface{}) {
	l.Panicfl(fmt.Sprintf(format, args...), l.newCtxFields())
}

func (l *fluentContextLogger) Errorf(format string, args ...interface{}) {
	l.Errorfl(fmt.Sprintf(format, args...), l.newCtxFields())
}

func (l *fluentContextLogger) Warnf(format string, args ...interface{}) {
	l.Warnfl(fmt.Sprintf(format, args...), l.newCtxFields())
}
func (l *fluentContextLogger) Infof(format string, args ...interface{}) {
	l.Infofl(fmt.Sprintf(format, args...), l.newCtxFields())
}

func (l *fluentContextLogger) Debugf(format string, args ...interface{}) {
	l.Debugfl(fmt.Sprintf(format, args...), l.newCtxFields())
}
func (l *fluentContextLogger) Debug(args ...interface{}) {
	l.Debugfl(fmt.Sprint(args...), l.newCtxFields())
}
func (l *fluentContextLogger) Error(args ...interface{}) {
	l.Errorfl(fmt.Sprint(args...), l.newCtxFields())
}
func (l *fluentContextLogger) Fatal(args ...interface{}) {
	l.Fatalfl(fmt.Sprint(args...), l.newCtxFields())
}
func (l *fluentContextLogger) Info(args ...interface{}) {
	l.Infofl(fmt.Sprint(args...), l.newCtxFields())
}
func (l *fluentContextLogger) Panic(args ...interface{}) {
	l.Panicfl(fmt.Sprint(args...), l.newCtxFields())
}
func (l *fluentContextLogger) Warn(args ...interface{}) {
	l.Warnfl(fmt.Sprint(args...), l.newCtxFields())
}

func (l *fluentContextLogger) GetLogrusLogger() *logrus.Logger {
	return l.flogger.GetLogrusLogger()
}

func (l *fluentContextLogger) decorateFromContext(fields Fields) {
	if ctxutil.GetOrderId(l.ctx) != "" {
		fields[keys.OrderId] = ctxutil.GetOrderId(l.ctx)
	}
	if ctxutil.GetEcomOrderId(l.ctx) != "" {
		fields[keys.EcomOrderId] = ctxutil.GetEcomOrderId(l.ctx)
	}
	if ctxutil.GetPlanDate(l.ctx) != "" {
		fields[keys.PlanDate] = ctxutil.GetPlanDate(l.ctx)
	}
	if ctxutil.GetEcomToken(l.ctx) != "" {
		fields[keys.EcomToken] = ctxutil.GetEcomToken(l.ctx)
	}
	if ctxutil.GetClientId(l.ctx) != "" {
		fields[keys.ClientId] = ctxutil.GetClientId(l.ctx)
	}
	if ctxutil.GetPaymentId(l.ctx) != "" {
		fields[keys.PaymentId] = ctxutil.GetPaymentId(l.ctx)
	}
}
