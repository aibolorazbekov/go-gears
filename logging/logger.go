package logging

import (
	"fmt"

	joonix "github.com/crystal-construct/log"
	lkh "github.com/gfremex/logrus-kafka-hook"
	"github.com/sirupsen/logrus"
)

func create(logLevel string) *logrus.Logger {
	logger := logrus.New()
	logger.Formatter = &joonix.FluentdFormatter{}
	joonix.UseStackdriverSeverity()
	level, err := logrus.ParseLevel(logLevel)
	if err == nil {
		logger.Level = level
	} else {
		fmt.Printf("Log level %s could not be parsed, setting 'info'\n", logLevel)
		logger.Level = logrus.InfoLevel
	}
	return logger
}

func createWithKafkaHook(logLevel string, brokers []string) *logrus.Logger {
	if len(brokers) == 0 {
		panic("kafka brokers should be at least one")
	}

	logger := logrus.New()

	hook, err := lkh.NewKafkaHook(
		"kh",
		[]logrus.Level{logrus.InfoLevel, logrus.WarnLevel, logrus.ErrorLevel},
		&joonix.FluentdFormatter{},
		brokers,
	)
	if err != nil {
		panic(err)
	}

	logger.Hooks.Add(hook)
	logger.Formatter = &joonix.FluentdFormatter{}
	joonix.UseStackdriverSeverity()
	level, err := logrus.ParseLevel(logLevel)
	if err == nil {
		logger.Level = level
	} else {
		fmt.Printf("Log level %s could not be parsed, setting 'info'\n", logLevel)
		logger.Level = logrus.InfoLevel
	}
	return logger
}
