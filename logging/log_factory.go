package logging

import (
	"context"
)

type ContextLogFactory interface {
	New(ctx context.Context) ContextLogger
}

type contextLogFactory struct {
	logger     FluentDLogger
	packageTag string
}

func NewContextLogFactory(logger FluentDLogger) ContextLogFactory {
	return &contextLogFactory{
		logger:     logger,
		packageTag: "",
	}
}

func (s *contextLogFactory) New(ctx context.Context) ContextLogger {
	if s.logger == nil {
		panic("attempted to context log without defining a logger")
	}
	return NewFluentContextLogger(ctx, s.logger)
}
