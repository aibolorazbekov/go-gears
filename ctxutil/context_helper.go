package ctxutil

import "context"

type contextKey string

const (
	OrderIdKey     = "orderId"
	EcomOrderIdKey = "ecomOrderId"
	EcomTokenKey   = "ecomToken"
	PlanDateKey    = "planDate"
	ClientIdKey    = "clientId"
	PaymentIdKey   = "paymentId"
)

func WithPaymentId(ctx context.Context, id string) context.Context {
	return withCtxValue(ctx, PaymentIdKey, id)
}

func GetPaymentId(ctx context.Context) string {
	return getCtxValue(ctx, PaymentIdKey)
}

func WithClientId(ctx context.Context, id string) context.Context {
	return withCtxValue(ctx, ClientIdKey, id)
}

func GetClientId(ctx context.Context) string {
	return getCtxValue(ctx, ClientIdKey)
}

func WithEcomToken(ctx context.Context, token string) context.Context {
	return withCtxValue(ctx, EcomTokenKey, token)
}

func GetEcomToken(ctx context.Context) string {
	return getCtxValue(ctx, EcomTokenKey)
}

func WithPlanDate(ctx context.Context, date string) context.Context {
	return withCtxValue(ctx, PlanDateKey, date)
}

func GetPlanDate(ctx context.Context) string {
	return getCtxValue(ctx, PlanDateKey)
}

func WithEcomOrderId(ctx context.Context, id string) context.Context {
	return withCtxValue(ctx, EcomOrderIdKey, id)
}

func GetEcomOrderId(ctx context.Context) string {
	return getCtxValue(ctx, EcomOrderIdKey)
}

func WithOrderId(ctx context.Context, id string) context.Context {
	return withCtxValue(ctx, OrderIdKey, id)
}

func GetOrderId(ctx context.Context) string {
	return getCtxValue(ctx, OrderIdKey)
}

func withCtxValue(ctx context.Context, key string, value string) context.Context {
	if value == "" {
		return ctx
	}
	return context.WithValue(ctx, contextKey(key), value)
}

func getCtxValue(ctx context.Context, key string) string {
	if ret := ctx.Value(contextKey(key)); ret != nil {
		return ret.(string)
	}
	return ""
}
